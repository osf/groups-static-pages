---
title: Running a hackathon
path: tips/hackathon-howto
---

This page is deprecated.  For further information on OpenStack Application Hackathons, please check the official hackathon page:

https://www.openstack.org/community/events/openstackhackathons
